import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { AdMobFree } from '@ionic-native/admob-free';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { TranslateLoader, TranslateStaticLoader} from 'ng2-translate/src/translate.service';
import { Http} from '@angular/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Globalization } from '@ionic-native/globalization';
import { IonicStorageModule } from '@ionic/storage';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { DataParkPage } from '../pages/data-park/data-park'; 
import { ListFavPage } from '../pages/list-fav/list-fav';
import { FavsPage } from '../pages/favs/favs';
import { MapFavPage } from '../pages/map-fav/map-fav';
import { SearchPage } from '../pages/search/search';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ParkProvider } from '../providers/park/park';
import { DataBaseProvider } from '../providers/data-base/data-base';
import { Map } from 'Leaflet';
import { Ionic2RatingModule } from 'ionic2-rating';

export function createTranslateLoader(http: Http){
  return new TranslateStaticLoader(http,'assets/i18n','.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DataParkPage,
    ListFavPage,
    MapFavPage,
    SearchPage

  

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      provide : TranslateLoader,
      useFactory : (createTranslateLoader),
      deps : [Http]
    }),
    HttpClientModule,
    IonicImageViewerModule,
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DataParkPage,
    ListFavPage,
    MapFavPage,
    SearchPage,
  

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    AdMobFree,
    HttpModule,
    HttpModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ParkProvider,
    HttpClient,
    OneSignal,
    SQLite,
    DataBaseProvider,
    Globalization
  ]
})
export class AppModule {}
