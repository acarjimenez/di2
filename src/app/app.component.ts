import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Http} from '@angular/http';
import { OneSignal } from '@ionic-native/onesignal';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { DataBaseProvider } from '../providers/data-base/data-base';
import { availableLanguages, sysOptions } from '../pages/home/home.constants';
import { TranslateService } from 'ng2-translate';




import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  languages = availableLanguages;
  selectedLanguage; //= sysOptions.systemLanguage;
  
  private translate: TranslateService;

  constructor(public platform: Platform, public statusBar: StatusBar, 
    public splashScreen: SplashScreen, public http:Http, public oneSignal: OneSignal,
    public alertCtrl: AlertController ,public sqlite:SQLite,
     public dataBaseProvider: DataBaseProvider, translate: TranslateService) {
    this.initializeApp();
    
    this.translate = translate;

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.createDataBasePark();
      this.notificaciones();
      this.applyLanguage();
    
    });
  }

  /**
   * Metodo que recibe una notificacion push, y muestra un alert con el mensaje.
   */
  private notificaciones(){

      this.oneSignal.startInit('ddd27922-400a-48c5-8326-dbe7b755e421', '412132225127');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
      this.oneSignal.handleNotificationOpened()
      .subscribe(jsonData => {
        let alert = this.alertCtrl.create({
          title: jsonData.notification.payload.title,
          subTitle: jsonData.notification.payload.body,
          buttons: ['OK']
        });
        alert.present();
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      });
      this.oneSignal.endInit();
    }

    /**
     *  Crea la base de datos de los parques.
     */
    public createDataBasePark(){

      this.sqlite.create({
        name: 'dataBase.db',
        location: 'default' // the location field is required
      })
      .then((db) => {
        console.log(db);
        this.dataBaseProvider.setDatabase(db);
        return this.dataBaseProvider.createTable();
      })
      .catch(error =>{
        console.log("ERROR CREATETABLE"+error);
      });
      

    }


/**
 * Traduce la aplicacion al lenguaje seleccionado
 */
    applyLanguage() {

      if(this.translate.getDefaultLang() !=null){

        this.selectedLanguage =  this.translate.getDefaultLang();
      } else {
        this.selectedLanguage = sysOptions.systemLanguage;
      }

      console.log("LENGUAJE: "+this.selectedLanguage);
      this.translate.use(this.selectedLanguage);
     // this.translate.setDefaultLang(this.selectedLanguage);
    }


    openPage(page) {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      this.nav.push(page);
    }


}
