import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';




const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token',
    'token' : 'Kq3U7CMEizNupXtcivvdI5HfXLKqEHVy9vRSQfhff2w',
    'API-KEY' : 'MJ09EV9EQWOfOcRc3sow78K0YqLpx1owcj_q_dPW-YE'
  })
};

@Injectable()
export class ParkProvider {
  

  constructor(public http: HttpClient) {
    
  }


/**
 * Obtiene todos los parques que hay entre los limites del mapa.
 * @param northWestLat 
 * @param northWestLng 
 * @param southEastLat 
 * @param southEastLng 
 */
  getParksCoords(northWestLat,northWestLng,southEastLat,southEastLng){

      
    var url = 'http://parques.ticarte.com/api/v1/search/place?latleft='+northWestLat+'&lonleft='+northWestLng+'x&latright='+southEastLat+'&lonright='+southEastLng;
    
    return new Promise(resolve =>{
      this.http.get(url,httpOptions).subscribe(data =>{
        resolve(data);
        console.log("url: "+url);
      }, 
      error =>{
        console.log("Error en provider: "+error)
      });
    });
  }


/**
 * Obtiene los comentarios de un parque.
 * @param id del parque.
 */
  getCommentsPark(id){

    var url = "http://parques.ticarte.com/api/v1/comments/"+id;

    return new Promise(resolve =>{

      this.http.get(url,httpOptions).subscribe( data =>{
        resolve(data);

      },
    error =>{
      console.log("ERROR GET COMMENTS "+error);
      });
    });

  }

/**
 * Obtiene la informacion de un parque
 * @param id del parque que se quiere obtener la informacion
 */
  getParkId(id){

    var url = "http://parques.ticarte.com/api/v1/place/"+id;

    return new Promise(resolve =>{

      this.http.get(url,httpOptions).subscribe( data =>{
        resolve(data);
        console.log("url: "+url);
      },
      error =>{
        console.log("ERROR EN GETPARKID"+error);
      });
    });

  }

/**
 * Obtiene la informacion de un parque a traves de la busqueda del nombre y los parametros
 * que se le pasa.
 * @param northWest 
 * @param southEast
 * @param nombre 
 * @param water 
 */
  getParkName(northWest,southEast,nombre,params){

    //ESTA MAL LA URL 
    var url ='http://parques.ticarte.com/api/v1/search?latleft='+northWest.lat+'&lonleft='+northWest.lng+'&latright='+southEast.lat+'&lonright='+southEast.lng+'&name='+name+'&potable_water='+'&toilets=All&restaurant=All&cafe=All&surveillance=All&pets_allowed=All&wifi=All';
  }



  getParkParams(northWest,southEast,params){

    var url = 'http://parques.ticarte.com/api/v1/search/place?latleft='+northWest.lat+'&lonleft='+northWest.lng+'&latright='+southEast.lat+'&lonright='+southEast.lng+params;
    
    return new Promise(resolve =>{
      
            this.http.get(url,httpOptions).subscribe( data =>{
              resolve(data);
      
            },
          error =>{
            console.log("ERROR PARAMS "+error);
            });
          });
  }


}
