import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';

/*
  Generated class for the DataBaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataBaseProvider {

  db: SQLiteObject = null;
  
  constructor(public http: HttpClient, public toastCtrl: ToastController) {
    console.log('Hello DataBaseProvider Provider');
  }

  setDatabase(db: SQLiteObject){
    if(this.db === null){
      this.db = db;
    }
  }

  /**
   * Crea la base de datos si no existe
   */
  createTable(){
    let sql = 'CREATE TABLE IF NOT EXISTS parks(id INTEGER PRIMARY KEY , name TEXT, latitude REAL, longitude REAL )';

    return this.db.executeSql(sql, []);
  }

/**
 * Recupera todos los parques de la base de datos.
 */
  getAllParks(){
    let sql = 'SELECT * FROM parks';
    return this.db.executeSql(sql, [])
    .then(response => {
      let parks = [];
      for (let index = 0; index < response.rows.length; index++) {
        parks.push( response.rows.item(index) );
      }
      return Promise.resolve( parks );
    })
    .catch(error => {
      console.log("ERROR GETALL"+error);
    })
  }

/**
 * Obtiene la informacion de un parque
 * @param id del parque a obtener
 */
  getParkById(id){
    let sql = 'SELECT * FROM parks WHERE id =?';
    return this.db.executeSql(sql,[id])
    .then(response =>{
      let park = response;
    })
    .catch(error =>{
      console.log("ERROR GETPARKBYID"+error);
    })
  }


  /**
   * Añade un parque a la base de datos.
   * @param data datos a guardar en la base de datos.
   */
  insert(data){
    for(let park of data){
    let sql = 'INSERT INTO parks(id, name, latitude, longitude) VALUES(?,?,?,?)';
    console.log("DATABASEINSERT: "+park.nid);
    console.log("LATITUD:"+park.location.latitude);
    return this.db.executeSql(sql, [park.nid, park.name, park.location.latitude, park.location.longitude])
    .then((result) =>{

      console.log("anadido correctamente"+result);
      
      let toast = this.toastCtrl.create({
      message: 'The park was added successfully',
      duration: 3000

      });
      toast.present();
    })

    .catch((error) =>{

      console.log("ERROR INSERT"+error);

      let toast = this.toastCtrl.create({
        message: 'The park is already saved as favorite',
        duration: 3000
      });
      toast.present();

    })
  }
  }
/**
 * Borra un parque de la base de datos.
 * @param id del parque a borrar.
 */
  delete(id){
    let sql = 'DELETE FROM parks WHERE id=?';
    return this.db.executeSql(sql, [id]);
  }


  
}

