import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, 
  LoadingController,ToastController } from 'ionic-angular';
import leaflet, { marker } from 'Leaflet';
import { Geolocation } from '@ionic-native/geolocation';
import { DataBaseProvider } from '../../providers/data-base/data-base';
import { DataParkPage } from '../data-park/data-park';
import { Storage } from '@ionic/storage';
import { empty } from 'rxjs/Observer';
import { TranslateService } from 'ng2-translate';


/**
 * Generated class for the MapFavPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-fav',
  templateUrl: 'map-fav.html',
})
export class MapFavPage {

  mapL: any;
  allParkFav: any;
  longitud: any;
  latitud: any;
  private translate: TranslateService;
  translated;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public geolocation: Geolocation, public dataBase: DataBaseProvider,
    public elementRef: ElementRef, public storage: Storage, public viewCtrl: ViewController,
    translate: TranslateService, public loadingCtrl:LoadingController,
    public toastCtrl: ToastController) {

    this.viewCtrl = this.navParams.get('viewCtrl');
    this.translate = translate;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapFavPage');

    this.storage.ready().then(() => {

      this.storage.get("latitud").then((latitud) => {
        this.latitud = latitud;
        //console.log("LATITUD: ", this.latitud);

        if (this.latitud != null) {

          this.storage.get("longitud").then((longitud) => {
            this.longitud = longitud;
            // console.log("LONGITUD: " + this.longitud);
            this.loadMap(this.latitud, this.longitud);
          })

        } else {

          this.getPosition();
        }
      })

    })
      .catch((error) => {

        console.log("ERROR MAP " + error);


      })

  }



  /**
  * Obtiene la posición en la cual se encuentra el dispositivo, devuelve
  * la latitud y la longitud del dispositivo.
  * 
  * Una vez obtenida la posición llama a loadMap(), para cargar un mapa
  * que se centra en la posición en la que se encuentra el dispositivo.
  */
  getPosition() {

    let options: { timeout: 10000, enableHighAccurary: true }

    this.geolocation.getCurrentPosition(options)
      .then((resp) => {

        // console.log(resp.coords.latitude, "------", resp.coords.longitude);
        this.loadMap(resp.coords.latitude, resp.coords.longitude);

      })

      .catch((error) => {
        this.translated = this.translate.get("location")["value"];
        
                let toast = this.toastCtrl.create({
                  message: this.translated,
                  duration: 3000,
                  position:"middle"

      });

      toast.present();

      })


  }


  /**
* Define un mapa, con un zoom por defecto, un zoom máximo, y uno minimo, y
* se centra en el punto que se le indica, en este caso la latitud, y longitud
* que se le pasa como parámetros a la función.
* Además llama a la función mostrarMarcadores() que en caso de que halla algún
*  punto de interés dentro de esa zona, se añade un marcador en ese punto.
* @param latitud 
* @param longitud 
*/
  loadMap(latitud, longitud) {

    this.mapL = leaflet.map('map', {
      center: leaflet.latLng(latitud, longitud),
      zoom: 15,
      minZoom: 6,
      maxZoom: 18
    });


    leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

    this.mapL.setView([latitud, longitud]);

    this.obtenerDatos();


  }

  /**
   * Obtiene todos los parques guardados en la base de datos, y los muestra en el mapa.
   */
  obtenerDatos() {

    this.dataBase.getAllParks()
      .then(result => {
        this.allParkFav = result;
        this.loadMarkers();
      })
      .catch(error => {
        console.error(error);
      });
  }


  /**
   * Añade un marcador por cada parque guardado en la base de datos.
   */
  loadMarkers() {

    for (let park of this.allParkFav) {

      var marker = leaflet.marker([park.latitude, park.longitude])
        .bindPopup('<a class="data-park" parkId="' + park.id + '">' + park.name + '</a>')
        .addTo(this.mapL);
      // console.log("park", park.id);
      //console.log("latitud", park.latitude);
      //console.log("longitud", park.longitude);

      let self = this;

      marker.on('popupopen', function () {
        self.elementRef.nativeElement.querySelector(".data-park")
          .addEventListener('click', (e) => {

            var id = park.id;
            self.openDataPark(id);
            //console.log("IDMAP: " + id)
          })

      })


    }

  }





  openDataPark(id) {

    this.navCtrl.push(DataParkPage, {
      parkId: id
    })

  }


  public onClickCancel() {
    this.viewCtrl.dismiss();
  }




}
