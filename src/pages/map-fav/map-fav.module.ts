import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapFavPage } from './map-fav';

@NgModule({
  declarations: [
    MapFavPage,
  ],
  imports: [
    IonicPageModule.forChild(MapFavPage),
  ],
})
export class MapFavPageModule {}
