import { NgModule } from '@angular/core';
import { IonicPageModule, Platform } from 'ionic-angular';
import { SettingsPage } from './settings';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { TranslateLoader, TranslateStaticLoader} from 'ng2-translate/src/translate.service';
import { Http } from '@angular/http'
import { HttpClient } from '@angular/common/http';
import { defaultLanguage, availableLanguages, sysOptions } from '../home/home.constants';
import { TranslateService } from 'ng2-translate';
import { Globalization } from '@ionic-native/globalization';


export function createTranslateLoader(http: Http){
  return new TranslateStaticLoader(http,'assets/i18n','.json');
}

@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    TranslateModule,
  ],
})
export class SettingsPageModule {}
