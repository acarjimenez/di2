import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { availableLanguages, sysOptions } from '../home/home.constants';
import { TranslateService } from 'ng2-translate';
import {Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { MapDefaultPage } from '../map-default/map-default';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  languages = availableLanguages;
  selectedLanguage = sysOptions.systemLanguage;
  
   translate: TranslateService;

  constructor(public navCtrl: NavController, public navParams: NavParams,
   translate :TranslateService,public http:Http) {
    this.translate = translate;


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }


  applyLanguage() {
    this.translate.use(this.selectedLanguage);
    this.translate.setDefaultLang(this.selectedLanguage);
  }
  
  openPage(page) {

    this.navCtrl.push(page);
  }

}
