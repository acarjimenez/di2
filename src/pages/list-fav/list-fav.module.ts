import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListFavPage } from './list-fav';

@NgModule({
  declarations: [
    ListFavPage,
  ],
  imports: [
    IonicPageModule.forChild(ListFavPage),
  ],
})
export class ListFavPageModule {}
