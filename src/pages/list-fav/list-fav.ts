import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ViewController } from 'ionic-angular';
import { DataParkPage } from '../data-park/data-park';
import { DataBaseProvider } from '../../providers/data-base/data-base';

/**
 * Generated class for the ListFavPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-fav',
  templateUrl: 'list-fav.html',
})
export class ListFavPage {

  allParkFav: any;
  allParkNumber: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public dataBase: DataBaseProvider, public alertCtrl: AlertController,
  public viewCtrl:ViewController) {

    this.viewCtrl = this.navParams.get('viewCtrl');

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListFavPage');

    this.mostrarDatos();
  }

  /**
   * Obtiene todos los datos guardados en la base de datos, y los muestra en la lista.
   */
  mostrarDatos(){

      this.dataBase.getAllParks()
      .then(result => {
        this.allParkFav = result;
        this.allParkNumber = this.allParkFav.length;
      })
      .catch( error => {
        console.error( error );
      });
    
  }

/**
 * Borra el parque 
 * @param id del parque a borrar.
 */
  deletePark(id){


    let alert = this.alertCtrl.create({
      title:'Delete',
      message: 'Are you sure to delete this park?',
      buttons: [
        {
          text: 'Yes',
          handler: ()=>{
            this.dataBase.delete(id)
            .then(()=>{
              this.mostrarDatos();
            })
            .catch((error)=>{
              console.log("ERRORDELETEPARK listFav"+error);
            })
            
          }
        },
        {
          text: 'No',
          handler: ()=>{

          }
        }
      ]
    });

  alert.present();    

  }



openDataPark(id){
  
  this.navCtrl.push(DataParkPage,{
    parkId : id
  })

} 


public onClickCancel() {
  this.viewCtrl.dismiss();
}



}
