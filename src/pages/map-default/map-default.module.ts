import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapDefaultPage } from './map-default';

@NgModule({
  declarations: [
    MapDefaultPage,
  ],
  imports: [
    IonicPageModule.forChild(MapDefaultPage),
  ],
})
export class MapDefaultPageModule {}
