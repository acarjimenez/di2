import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import leaflet, { marker } from 'Leaflet';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/**
 * Generated class for the MapDefaultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-default',
  templateUrl: 'map-default.html',
})
export class MapDefaultPage {

  mapL: any;
  layer = leaflet.featureGroup(); //Layer donde se añaden todos los marcadores.
  latitud;
  longitud;
  private translate: TranslateService;
  translated; 

  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation:
    Geolocation, public storage: Storage, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, translate: TranslateService) {

      this.translate = translate;

  }

  ionViewDidLoad() {

    this.translated = this.translate.get("loading")["value"];

    let loading = this.loadingCtrl.create({
      content: this.translated
    });

    loading.present();
    console.log('ionViewDidLoad MapDefaultPage');
    this.obtenerPosicion();

    loading.dismiss();
  }



  /**
  * Obtiene la posición en la cual se encuentra el dispositivo, devuelve
  * la latitud y la longitud del dispositivo.
  * 
  * Una vez obtenida la posición llama a loadMap(), para cargar un mapa
  * que se centra en la posición en la que se encuentra el dispositivo.
  */
  obtenerPosicion() {


    let options: { timeout: 10000, enableHighAccurary: true }

    this.geolocation.getCurrentPosition(options)
      .then((resp) => {

        this.loadMap(resp.coords.latitude, resp.coords.longitude);
        
      })
      .catch((error) =>{
        
        console.log("ERROR DEFAULT: ",error);
        this.translated = this.translate.get("location")["value"];
        
                let toast = this.toastCtrl.create({
                  message: "dsfasdfasdf",
                  duration: 3000,
                  position:"middle"


      })


      toast.present();

  })
}


  /**
* Define un mapa, con un zoom por defecto, un zoom máximo, y uno minimo, y
* se centra en el punto que se le indica, en este caso la latitud, y longitud
* que se le pasa como parámetros a la función.
* Además llama a la función mostrarMarcadores() que en caso de que halla algún
*  punto de interés dentro de esa zona, se añade un marcador en ese punto.
* @param latitud 
* @param longitud 
*/
  loadMap(latitud, longitud) {

    this.mapL = leaflet.map('map', {
      center: leaflet.latLng(latitud, longitud),
      zoom: 15,
      minZoom: 6,
      maxZoom: 18
    });


    leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

    this.mapL.setView([latitud, longitud]);

    this.loadData();

    this.mapL.on('click', (e) => { this.onMapClick(e) });

  }

  //Recibe el evento al hacer click en el mapa
  onMapClick(e) {


    this.layer.clearLayers();
    var marker = leaflet.marker([e.latlng.lat, e.latlng.lng]);
    //.addTo(this.mapL);
    this.layer.addLayer(marker);
    this.layer.addTo(this.mapL);

    var latitud = e.latlng.lat;
    var longitud = e.latlng.lng;
    console.log("latitud" + e.latlng.lat, "longitud" + e.latlng.lng);

    this.saveData(latitud, longitud);



  }


  saveData(latitud, longitud) {

    this.storage.set("latitud", latitud);
    this.storage.set("longitud", longitud);

    let toast = this.toastCtrl.create({
      message: 'Default location saved successfully',
      duration: 3000
    });
    toast.present();

  }


  loadData() {

    this.storage.ready().then(() => {

      this.storage.get("latitud").then((latitud) => {
        this.latitud = latitud;
        console.log("LATITUD: ", this.latitud);

        if(this.latitud !=null){

        this.storage.get("longitud").then((longitud) => {
          this.longitud = longitud;
          console.log("LONGITUD: " + this.longitud);

          var marker = leaflet.marker([this.latitud,this.longitud]);
          this.layer.addLayer(marker);
          this.layer.addTo(this.mapL);

        })

      }

      })
      .catch((error)=>{

        console.log("ERROR LOADDATA ",error);
      })
    

    })
  }



}
