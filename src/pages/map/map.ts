import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import leaflet, { marker } from 'Leaflet';
import { DataParkPage } from '../data-park/data-park';
import { Storage } from '@ionic/storage';
import { ParkProvider } from '../../providers/park/park';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the MapAroundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  mapL: any;
  latitud: any;
  longitud: any;
  northWest;
  southEast;
  allParks;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public parkProvider: ParkProvider,
    public elementRef: ElementRef, public loadingCtrl: LoadingController,
    public geolocation: Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');

    let loading = this.loadingCtrl.create({
      content: ""
    });

    loading.present();

    this.storage.ready().then(() => {

      this.storage.get("latitud").then((latitud) => {
        this.latitud = latitud;
        console.log("LATITUD: ", this.latitud);

        if (this.latitud != null) {

          this.storage.get("longitud").then((longitud) => {
            this.longitud = longitud;
            console.log("LONGITUD: " + this.longitud);
            this.loadMap(this.latitud, this.longitud);
          })

        } else {

          this.getPosition();
        }
      })

    })
      .catch((error) => {

        console.log("ERROR MAP " + error);


      })

    loading.dismiss();

  }


  /**
  * Define un mapa, con un zoom por defecto, un zoom máximo, y uno minimo, y
  * se centra en el punto que se le indica, en este caso la latitud, y longitud
  * que se le pasa como parámetros a la función.
  * Además llama a la función mostrarMarcadores() que en caso de que halla algún
  *  punto de interés dentro de esa zona, se añade un marcador en ese punto.
  * @param latitud 
  * @param longitud 
  */
  loadMap(latitud, longitud) {

    this.mapL = leaflet.map('map', {
      center: leaflet.latLng(latitud, longitud),
      zoom: 15,
      minZoom: 6,
      maxZoom: 18
    });


    leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

    this.mapL.setView([latitud, longitud]);

    this.getBoundsMap();
    this.getParksCoord();

  }


  /**
* Obtiene los puntos cardinales de la región del mapa que estamos viendo.
* En este caso solo obtenemos el noroeste, y el sureste, que son los puntos
* que nos interesa conocer.
*/
  getBoundsMap() {

    var bounds = this.mapL.getBounds();

    this.northWest = bounds.getNorthWest();
    console.log("norte: " + this.northWest);

    this.southEast = bounds.getSouthEast();
    console.log("sur: " + this.southEast);

  }




  /**
* Hace una llamada al provider park, el cual le pasa la latitud y longitud
* del noroeste y del sureste del mapa, y devuelve todos los lugares que nos
* interesa que estén dentro de la zona del mapa que estemos viendo.
* 
* A continuación, llama a la función loadMarkers(), para añadir un marcador
* en esos lugares.
*/
  getParksCoord() {

    this.parkProvider.getParksCoords(this.northWest.lat, this.northWest.lng,
      this.southEast.lat, this.southEast.lng)
      .then(data => {
        this.allParks = data;
        console.log(data);

        this.loadMarkers();


      })
      .catch(error => {
        console.log("Error en home.ts provider: " + error);

      })
  }



  /**
* Añade un marcador en la zona que se le indique.
*/
  loadMarkers() {



    leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

    for (let datos of this.allParks) {
      var marker = leaflet.marker([datos.location.latitude, datos.location.longitude])
        .bindPopup('<a class="data-park" parkId="' + datos.id + '">' + datos.name + '</a>')
        .addTo(this.mapL);

      let self = this;

      marker.on('popupopen', function () {
        self.elementRef.nativeElement.querySelector(".data-park")
          .addEventListener('click', (e) => {

            var id = datos.nid;
            self.openDataPark(id);
            console.log("IDMAP: " + id)
          })

      })




    }

  }



  openDataPark(id) {

    this.navCtrl.push(DataParkPage, {
      parkId: id
    })

  }


  /**
* Obtiene la posición en la cual se encuentra el dispositivo, devuelve
* la latitud y la longitud del dispositivo.
* 
* Una vez obtenida la posición llama a loadMap(), para cargar un mapa
* que se centra en la posición en la que se encuentra el dispositivo.
*/
  getPosition() {

    let options: { timeout: 10000, enableHighAccurary: true }

    this.geolocation.getCurrentPosition(options)
      .then((resp) => {

        this.latitud = resp.coords.latitude;
        this.longitud = resp.coords.longitude;

        console.log(this.latitud + "-------" + this.longitud);
        this.loadMap(this.latitud, this.longitud);
      })

      .catch((error) => {
        console.log("error: " + error)
      })


  }







}
