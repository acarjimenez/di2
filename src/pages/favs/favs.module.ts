import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavsPage } from './favs';
import { ListFavPage } from '../list-fav/list-fav';
import { MapFavPage } from '../map-fav/map-fav';

@NgModule({
  declarations: [
    FavsPage,
    //ListFavPage,
    //MapFavPage
  ],
  imports: [
    IonicPageModule.forChild(FavsPage),
  ],
  entryComponents: [
    //ListFavPage,
    //MapFavPage
  ]
})
export class FavsPageModule {}
