import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ListFavPage } from '../list-fav/list-fav';
import { MapFavPage } from '../map-fav/map-fav';

/**
 * Generated class for the FavsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favs',
  templateUrl: 'favs.html',
})
export class FavsPage {
  data: any;


  listFavPage =  ListFavPage;
  mapFavPage = MapFavPage;

  constructor(public viewCtrl: ViewController ){
    
    this.data = {
      viewCtrl: this.viewCtrl
    }

  }

 

}


