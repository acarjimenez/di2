import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ParkProvider } from '../../providers/park/park';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  northWest: any;
  southEast: any;
  parks;
  nombre;
  params;
  water: boolean = false;
  wifi: boolean = false;
  cafe: boolean = false;
  restaurant: boolean = false;
  pets: boolean = false;
  surveillance: boolean = false;
  toilets: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public parkProvider: ParkProvider,public viewCtrl: ViewController) {

    this.northWest = this.navParams.get("northWest");
    this.southEast = this.navParams.get("SouthEast");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }


  searchParks() {

    this.getParams();

    if (this.nombre == null) {

      this.parkProvider.getParkParams(this.northWest, this.southEast, this.params)
      .then(data =>{
        this.parks = data;

        this.dismiss();

      })

      .catch(error=>{
        console.log("ERROR EN SEARCHPARKS NOMBRE=NULL ",error);
      })

    }

    else {

      this.parkProvider.getParkName(this.northWest,this.southEast,this.nombre,this.params);
    }


  }


  getParams() {

    this.params = "";

    if (this.water == true) {

      this.params = this.params + "&potable_water=1";
    } else {
      this.params = this.params + "&potable_water=0";
    }

    if (this.toilets == true) {

      this.params = this.params + "&toilets=1";
    } else {
      this.params = this.params + "&toilets=0";
    }

    if(this.restaurant ==true){
      
      this.params = this.params+"&restaurant=1";
    } else {
      this.params = this.params+"&restaurant=0";
    }

    if(this.cafe ==true){
      
      this.params = this.params+"&cafe=1";
    } else {
     this.params = this.params+"&cafe=0";
    }

    if(this.surveillance ==true){
      
      this.params = this.params+"&surveillance=1";
    } else {
      this.params = this.params+"&surveillance=0";
    }

    if(this.pets ==true){
      
      this.params = this.params+"&pets_allowed=1";
    } else {
      this.params = this.params+"&pets_allowed=0";
    }

    if(this.wifi ==true){
      
      this.params = this.params+"&wifi=1";
    } else {
      this.params = this.params+"&wifi=0";
    }

  }

  dismiss(){

    this.viewCtrl.dismiss(this.parks);
  }

}
