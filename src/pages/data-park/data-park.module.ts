import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataParkPage } from './data-park';

@NgModule({
  declarations: [
    DataParkPage,
  ],
  imports: [
    IonicPageModule.forChild(DataParkPage)
  ],
})
export class DataParkPageModule {}
