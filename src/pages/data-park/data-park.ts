import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ParkProvider } from '../../providers/park/park';
import { DataBaseProvider } from '../../providers/data-base/data-base';
import { AdMobFree, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';


/**
 * Generated class for the DataParkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-data-park',
  templateUrl: 'data-park.html',
})

export class DataParkPage {

  parkData;
  parkId;
  images =[];
  comments;
  numComments: number;
  rate: number;
  seeComments: boolean;
 

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public parkProvider: ParkProvider, public dataBaseProvider: DataBaseProvider,
  public loadingCtrl: LoadingController,public admob: AdMobFree) {
    
    this.showInterstitial();

    this.parkId = navParams.get("parkId");
    //console.log("ID"+this.parkId);
    this.seeComments = true;

    this.getParkById(this.parkId);


    
    
  }

 

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataParkPage');
  
  }

/**
 * Muestra un anuncio
 */
  showInterstitial(){

    const interstitialConfig: AdMobFreeInterstitialConfig= {
      isTesting: true,
      autoShow: true
    }
    this.admob.interstitial.config(interstitialConfig);

    this.admob.interstitial.prepare()
    .then(() => {
      //Como hemos configurado el banner para que se muestre automaticamente,
      //aqui no hace falta añadir la función banner.show().
   
    
    })
      
    .catch (e => 
      console.log("ERROR Interstitial: "+e)
    );

  }


/**
 * Se le pasa el id del parque que se quiere consultar, y se obtienen sus datos.
 * Mientras los obtiene,muestra un loading.
 * @param id 
 */
  getParkById(id){

    let loading = this.loadingCtrl.create({
      content: ""
    });

    loading.present();
    
        this.parkProvider.getParkId(id)
        .then(data =>{
          this.parkData = data;
  

          for(let datos of this.parkData){
            this.rate = datos["evaluation"];
          
          }


  })
        .catch(error =>{
          console.log("Error en DATAPARK.ts GETPARKID: "+error);
      });

      this.parkProvider.getCommentsPark(id)
      .then(data =>{
        this.comments = data;
        this.numComments = this.comments.length;
        
      })

      .catch(error =>{
        console.log("ERROR OBTENER COMENTS DATAPARK "+error)
      });

      loading.dismiss();

      }


      /**
       * Guarda en la base de datos la informacion del parque.
       */
anadirFavoritos(){

  this.dataBaseProvider.insert(this.parkData);


}


showComments(){

  if(this.seeComments ==true){

    this.seeComments = false;

  } else {
    this.seeComments = true;
  }
}

}




