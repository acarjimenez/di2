export const availableLanguages = [{
	code: 'en',
	name: 'English'
}, {
	code: 'es',
	name: 'Español'
}];


export const defaultLanguage = 'es';

export const sysOptions = {
	systemLanguage: defaultLanguage
};