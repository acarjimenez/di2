import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Http} from '@angular/http';
import { availableLanguages, sysOptions } from './/home.constants';
import { TranslateService } from 'ng2-translate';
import { HttpClient } from '@angular/common/http';

import { MapPage } from '../../pages/map/map';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Nav) nav: Nav;

  languages = availableLanguages;
  //selectedLanguage; //= sysOptions.systemLanguage;

  
  private translate: TranslateService;


  constructor(public platform: Platform, public statusBar: StatusBar, 
    public splashScreen: SplashScreen, public http:Http, public navCtrl: NavController,
   translate: TranslateService) {
    this.translate = translate;




  }

  applyLanguage() {
    this.translate.use(this.translate.getDefaultLang());

  }
  




  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.navCtrl.push(page);

    let value = this.translate.get("home");
    //console.log("TRADUCCION: ",value['value']);
    console.log("TRADUCCION: ",this.translate.get("home")["value"]);
  }
}
