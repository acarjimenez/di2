import { NgModule } from '@angular/core';
import { IonicPageModule, Platform } from 'ionic-angular';
import { HomePage } from './home';
import { SettingsPage } from '../settings/settings';
import { ListFavPage } from '../list-fav/list-fav';
import { FavsPage } from '../favs/favs';
import { MapFavPage } from '../map-fav/map-fav';
import { MapDefaultPage } from '../map-default/map-default';
import { MapPage } from '../map/map';
import { MapAroundPage } from '../map-around/map-around';
import { MapSearchPage } from '../map-search/map-search';
import { defaultLanguage, availableLanguages, sysOptions } from '../home/home.constants';
import { TranslateService } from 'ng2-translate';
import { Globalization } from '@ionic-native/globalization';

@NgModule({
  declarations: [
    HomePage,  
    MapPage,
    SettingsPage,
    ListFavPage,
    FavsPage,
		MapFavPage,
    MapDefaultPage,
    MapAroundPage,
    MapSearchPage


  ],
  imports: [
    IonicPageModule.forChild(HomePage)
  ],
  entryComponents: [
    SettingsPage,
    MapPage,
		FavsPage,
    MapDefaultPage,
    MapAroundPage,
    MapSearchPage



  ],
})
export class MapPageModule {}
