import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapAroundPage } from './map-around';
import { DataParkPage } from '../data-park/data-park';

@NgModule({
  declarations: [
    MapAroundPage,

  ],
  imports: [
    IonicPageModule.forChild(MapAroundPage)
  ],
  entryComponents: [

]

})
export class MapPageModule {}
