import { Component,  ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import leaflet, { marker } from 'Leaflet';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';
import { ParkProvider} from '../../providers/park/park';
import { Geolocation } from '@ionic-native/geolocation';
import { DataParkPage } from '../data-park/data-park';


/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-around',
  templateUrl: 'map-around.html',
})
export class MapAroundPage {

  mapL: any;
  latitud;
  longitud;
  northWest;
  southEast;
  allParks;
  layer = leaflet.featureGroup();


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public admob: AdMobFree, public parkProvider: ParkProvider, public geolocation:
  Geolocation, public elementRef: ElementRef, public loadingCtrl: LoadingController) {

   
    this.banner();
  }


  ionViewDidLoad() {

    console.log('ionViewDidLoad MapAroundPage');

    let loading = this.loadingCtrl.create({
      content: ""
    });
  
    loading.present();


    this.getPosition();

    loading.dismiss();

  }




/**
* Define un mapa, con un zoom por defecto, un zoom máximo, y uno minimo, y
* se centra en el punto que se le indica, en este caso la latitud, y longitud
* que se le pasa como parámetros a la función.
* Además llama a la función mostrarMarcadores() que en caso de que halla algún
*  punto de interés dentro de esa zona, se añade un marcador en ese punto.
* @param latitud 
* @param longitud 
*/
  loadMap(latitud,longitud){

    this.mapL = leaflet.map('map',{
      center: leaflet.latLng(latitud,longitud),
      zoom:15,
       minZoom: 6, 
       maxZoom: 18
   });
 
 
   leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);
 
   this.mapL.setView([latitud, longitud]);
 
   this.getBoundsMap();
   this.getParksCoord();
 
   }
    


/**
* Muestra un banner de publicidad, en la parte de abajo de la pantalla.
*/
banner(){
  
const bannerConfig : AdMobFreeBannerConfig = { 

isTesting: true,
autoShow: true
};
  
this.admob.banner.config(bannerConfig);
  
this.admob.banner.prepare()
.then(() => {
  //Como hemos configurado el banner para que se muestre automaticamente,
  //aqui no hace falta añadir la función banner.show().


})
  
.catch (e => 
  console.log("ERROR banner: "+e)
);

}



/**
 * Obtiene los puntos cardinales de la región del mapa que estamos viendo.
 * En este caso solo obtenemos el noroeste, y el sureste, que son los puntos
 * que nos interesa conocer.
 */
  getBoundsMap(){
  
      var bounds = this.mapL.getBounds();
  
      this.northWest = bounds.getNorthWest();
      console.log("norte: "+this.northWest);
  
      this.southEast = bounds.getSouthEast();
      console.log("sur: "+ this.southEast);
  
  }




  
/**
 * Función que busca parques en función de los límites del mapa, y añade marcadores donde 
 * halla algún parque.
 */
searchParks(){

  let loading = this.loadingCtrl.create({
    content: ""
  });

  loading.present();
  this.getBoundsMap();
  this.getParksCoord();

  loading.dismiss();
}





/**
 * Añade un marcador en la zona que se le indique.
 */
loadMarkers(){

this.layer.clearLayers();
  
  leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

  for(let datos of this.allParks){
   var marker = leaflet.marker([datos.location.latitude , datos.location.longitude])
    .bindPopup('<a class="data-park" parkId="'+datos.id+'">'+datos.name+'</a>');
    //.addTo(this.mapL);
    this.layer.addLayer(marker);

    let self = this;

    marker.on('popupopen',function() {
       self.elementRef.nativeElement.querySelector(".data-park")
        .addEventListener('click',(e) =>{

          var id = datos.nid;
          self.openDataPark(id);
          console.log("IDMAP: "+id)
        })

    })

    
    

  }

  this.layer.addTo(this.mapL);

}



/**
* Obtiene la posición en la cual se encuentra el dispositivo, devuelve
* la latitud y la longitud del dispositivo.
* 
* Una vez obtenida la posición llama a loadMap(), para cargar un mapa
* que se centra en la posición en la que se encuentra el dispositivo.
*/
getPosition(){
  
  let options: {timeout:10000, enableHighAccurary:true}
  
  this.geolocation.getCurrentPosition(options)
    .then ((resp) =>{
  
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
  
      console.log(this.latitud+"-------"+this.longitud);
      this.loadMap(this.latitud,this.longitud);
      })
  
    .catch((error) =>{
      console.log("error: "+error)
      })
  
  
    }




/**
 * Hace una llamada al provider park, el cual le pasa la latitud y longitud
 * del noroeste y del sureste del mapa, y devuelve todos los lugares que nos
 * interesa que estén dentro de la zona del mapa que estemos viendo.
 * 
 * A continuación, llama a la función loadMarkers(), para añadir un marcador
 * en esos lugares.
 */
getParksCoord(){
  
      this.parkProvider.getParksCoords(this.northWest.lat,this.northWest.lng,
      this.southEast.lat,this.southEast.lng)
        .then(data =>{
          this.allParks = data;
          console.log(data);
  
          this.loadMarkers();


        })
        .catch(error =>{
            console.log("Error en home.ts provider: "+error);

        })        
    }




openDataPark(id){
  
  this.navCtrl.push(DataParkPage,{
    parkId : id
  })

}    

}
