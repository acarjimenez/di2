import { Component, ElementRef } from '@angular/core';
import {
  IonicPage, NavController, NavParams, ModalController,
  LoadingController, ToastController
} from 'ionic-angular';
import leaflet, { marker } from 'Leaflet';
import { ParkProvider } from '../../providers/park/park';
import { Geolocation } from '@ionic-native/geolocation';
import { DataParkPage } from '../data-park/data-park';
import { SearchPage } from '../search/search';
import { TranslateService } from 'ng2-translate';


/**
 * Generated class for the MapSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-search',
  templateUrl: 'map-search.html',
})
export class MapSearchPage {

  mapL: any;
  latitud;
  longitud;
  northWest;
  southEast;
  allParks;
  layer = leaflet.featureGroup();
  private translate: TranslateService;
  translated;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public parkProvider: ParkProvider,
    public geolocation: Geolocation, public loadingCtrl: LoadingController,
    public elementRef: ElementRef, translate: TranslateService,
    public toastCtrl: ToastController) {

    this.translate = translate;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapSearchPage');

    this.translated = this.translate.get("loading")["value"];

    let loading = this.loadingCtrl.create({
      content: this.translated
    });

    loading.present();


    this.getPosition();

    loading.dismiss();


  }


  /**
* Define un mapa, con un zoom por defecto, un zoom máximo, y uno minimo, y
* se centra en el punto que se le indica, en este caso la latitud, y longitud
* que se le pasa como parámetros a la función.
* Además llama a la función mostrarMarcadores() que en caso de que halla algún
*  punto de interés dentro de esa zona, se añade un marcador en ese punto.
* @param latitud 
* @param longitud 
*/
  loadMap(latitud, longitud) {

    this.mapL = leaflet.map('map', {
      center: leaflet.latLng(latitud, longitud),
      zoom: 15,
      minZoom: 6,
      maxZoom: 18
    });


    leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

    this.mapL.setView([latitud, longitud]);

    //this.getBoundsMap();


  }




  /**
* Obtiene los puntos cardinales de la región del mapa que estamos viendo.
* En este caso solo obtenemos el noroeste, y el sureste, que son los puntos
* que nos interesa conocer.
*/
  getBoundsMap() {

    var bounds = this.mapL.getBounds();

    this.northWest = bounds.getNorthWest();
    console.log("norte: " + this.northWest);

    this.southEast = bounds.getSouthEast();
    console.log("sur: " + this.southEast);

  }




  /**
* Obtiene la posición en la cual se encuentra el dispositivo, devuelve
* la latitud y la longitud del dispositivo.
* 
* Una vez obtenida la posición llama a loadMap(), para cargar un mapa
* que se centra en la posición en la que se encuentra el dispositivo.
*/
  getPosition() {

    let options: { timeout: 10000, enableHighAccurary: true }

    this.geolocation.getCurrentPosition(options)
      .then((resp) => {

        this.latitud = resp.coords.latitude;
        this.longitud = resp.coords.longitude;

        console.log(this.latitud + "-------" + this.longitud);
        this.loadMap(this.latitud, this.longitud);
      })

      .catch((error) => {

        this.translated = this.translate.get("location")["value"];

        let toast = this.toastCtrl.create({
          message: this.translated,
          duration: 3000,
          position: "middle"

        });

        toast.present();

      });


  }





  openDataPark(id) {

    this.navCtrl.push(DataParkPage, {
      parkId: id
    })

  }



  /**
   * Añade un marcador en la zona que se le indique.
   */
  loadMarkers() {



    leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.mapL);

    this.layer.clearLayers();

    for (let datos of this.allParks) {
      var marker = leaflet.marker([datos.location.latitude, datos.location.longitude])
        .bindPopup('<a class="data-park" parkId="' + datos.id + '">' + datos.name + '</a>');
      this.layer.addLayer(marker);
      // .addTo(this.mapL);

      let self = this;

      marker.on('popupopen', function () {
        self.elementRef.nativeElement.querySelector(".data-park")
          .addEventListener('click', (e) => {

            var id = datos.nid;
            self.openDataPark(id);
            console.log("IDMAP: " + id)
          })

      })


    }

    this.layer.addTo(this.mapL);

  }



  showModal() {

    this.getBoundsMap();

    let modal = this.modalCtrl.create(SearchPage, {
      "northWest": this.northWest,
      "SouthEast": this.southEast
    });

    modal.present();

    modal.onDidDismiss(data => {

      let loading = this.loadingCtrl.create({
        content: ""
      });

      loading.present();

      if(data !=null){
      this.allParks = data;

      this.loadMarkers();
      
    }

      loading.dismiss();
    })
  }



}
